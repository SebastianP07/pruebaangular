# PruebaAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.22.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

-------------------------------------------------------------------------------------------------------------------------------------------

## EJERCICIO DE EJEMPLO

- https://cli.angular.io/
- https://loiane.com/2017/08/how-to-add-bootstrap-to-an-angular-cli-project/
- https://www.techiediaries.com/angular-bootstrap/

## Instalación de Angular
	verificar que este instalado Node.js en su version mas estable 
	npm install -g @angular/cli
	ng new my-dream-app
	cd my-dream-app
	ng serve

## Instalacion Boostrap

Version especifica
	npm install bootstrap@3.3.7

Ultima version
	npm install bootstrap

Configurar angular.json:
	"styles": [
	  "node_modules/bootstrap/dist/css/bootstrap.min.css",
	  "styles.scss"
	]

Alternativa: CSS de Bootstrap local
	Si agregó el archivo CSS Bootstrap localmente, simplemente impórtelo en angular.json

	"styles": [
	  "styles/bootstrap-3.3.7-dist/css/bootstrap.min.css",
	  "styles.scss"
	],


Componentes JavaScript Bootstrap con ngx-bootstrap
	npm install ngx-bootstrap --save

Boostrap y ngx-boostrap en un solo paso
	npm install bootstrap ngx-bootstrap --save


Si se quiere un componente global en la aplicacion se importan y exportan los componentes de angular
-------------------------------------------------------------------------------------------------------
	import { NgModule } from '@angular/core';
	import { CommonModule } from '@angular/common';

	import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
	import { TooltipModule } from 'ngx-bootstrap/tooltip';
	import { ModalModule } from 'ngx-bootstrap/modal';

	@NgModule({
	  imports: [
	    CommonModule,
	    BsDropdownModule.forRoot(),
	    TooltipModule.forRoot(),
	    ModalModule.forRoot()
	  ],
	  exports: [BsDropdownModule, TooltipModule, ModalModule]
	})
	export class AppBootstrapModule {}
-------------------------------------------------------------------------------------------------------


Por último, importar tu módulo de arranque en ti app.module.ts.
-------------------------------------------------------------------------------------------------------
	import { AppBootstrapModule } from './app-bootstrap/app-bootstrap.module';

	@NgModule({
	  imports: [BrowserModule, AppBootstrapModule],
	  // ...
	})
	export class AppModule {}
-------------------------------------------------------------------------------------------------------


Ejemplos pagina oficial ngx-bootstrap
- https://valor-software.com/ngx-bootstrap/#/documentation#getting-started
- https://valor-software.com/ngx-bootstrap/#/carousel

## angular-fontawesome
- https://www.npmjs.com/package/@fortawesome/angular-fontawesome