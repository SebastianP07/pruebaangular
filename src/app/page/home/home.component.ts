import { Component, OnInit, TemplateRef, Inject } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private modalService: BsModalService) { }

  public modalRef: BsModalRef;

  // tslint:disable-next-line:member-ordering
  slideChangeMessage = '';

  slides  = [
    {image: 'https://i.pinimg.com/originals/5a/d2/40/5ad2400f5557fb35e43fe01598c284ba.jpg'},
    // tslint:disable-next-line:max-line-length
    {image: 'http://2.bp.blogspot.com/-80Ua9ZGKKFM/T7PjsvnpujI/AAAAAAAAN1w/XozODZw5HZ8/s1600/Fondos+de+pantalla+con+bellos+rincones+de+la+naturaleza+(145).jpg'},
    {image: 'https://d500.epimg.net/cincodias/imagenes/2020/02/06/pyme/1581021638_329752_1581021985_noticia_normal_recorte1.jpg'}
  ];

  log(event: number) {
    this.slideChangeMessage = `Slide has been switched: ${event}`;
  }

  ngOnInit() {
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
