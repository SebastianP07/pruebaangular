import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { ComponentsGlobalModule } from './components-global/components-global.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './page/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsGlobalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
